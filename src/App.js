import React, { Component } from 'react';
import DateWidget from './components/DateWidget';
import logo from './logo.svg';
import moment from 'moment-timezone';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <DateWidget
          timezoneFormat="Z"
          dateFormat="dddd DD MMMM YYYY"
          from={moment().add(-5,'day')}
          to={moment().add(5,'day')}
        />
      </div>
    );
  }
}

export default App;
